﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace stringsApp
{
  public class Program
  {
    public static async Task Main(string[] args)
    {
      //Find duplicate characters in a string.
      const string stringToManipulate = "abbcdedfzdfdmemsd";
      /* Console.WriteLine($"The duplicate characters in a string '{stringToManipulate}': {StringManipulation.FindDuplicateCharacters(stringToManipulate)} ");
       Console.WriteLine();

       //Get all unique characters in a string.
       Console.WriteLine($"The unique characters in a string '{stringToManipulate}': {StringManipulation.FindUniqueCharacters(stringToManipulate)} ");
       Console.WriteLine();

       //Reverse a string.
       Console.WriteLine($"The reversed string '{stringToManipulate}': {StringManipulation.ReverseString(stringToManipulate)}");
       Console.WriteLine();

       const string sentence = "Hi there. My name is Siyabonga";
       //Reverse each word of the sentence.
       Console.WriteLine($"The reversed sentence '{sentence}': {StringManipulation.ReverseEachWord(sentence)}");
       Console.WriteLine();

       //Count each word of the sentence.
       Console.WriteLine($"Number of words in the sentence'{sentence}': {StringManipulation.CountEachWord(sentence)}");
       Console.WriteLine();

       //Check if a string is a palindrome or not.
       var words = System.IO.File.ReadAllLines("PalindromeWords.txt");
       foreach (var word in words)
       {
         Console.WriteLine($"Is '{word}' a palindrome: {StringManipulation.IsPalindrome(word)}");
       }
       Console.WriteLine();


      //Check max occurrence of a character in the string.
      const string stringToManipulate1 = "aaaabbccccdddd";
      Console.WriteLine($"The max occurrence(s) of a character in the string '{stringToManipulate1}' : {StringManipulation.MaxOccurrence(stringToManipulate1)}");
      Console.WriteLine();

      //Get all possible substring in a string..
      const string stringToManipulate2 = "india";
      Console.WriteLine($"all possible substring in the string '{stringToManipulate2}' : {StringManipulation.GetPossibleSubstring(stringToManipulate2)}");
      Console.WriteLine();

      //Get the first char of each word in capital letter.
      const string stringToManipulate2 = "hi there. my name is siyabonga";
      Console.WriteLine($"The first char of each word in capital letter in the string'{stringToManipulate2}' : {StringManipulation.ConvertFirstLetterToUpper(stringToManipulate2)}");
      Console.WriteLine();

      //Given two numbers as strings s1 and s2. Calculate their Product.
      const string s1 = "505041410988047";
      const string s2 = "3318139";
      Console.WriteLine($"The product of the 2 strings'{s1}' and '{s2}' : {StringManipulation.MultiplyStrings(s1, s2)}");
      Console.WriteLine();*/

      //Swap two strings.
      const string s1 = "abc";
      const string s2 = "def";
      Console.WriteLine($"The list of swapped strings'{s1}' and '{s2}' :");
      StringManipulation.SwapStrings(s1, s2);

    }
  }
}
