﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace stringsApp
{
  public static class StringManipulation
  {
    public static string FindDuplicateCharacters(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return "";
      }

      stringToManipulate = stringToManipulate.ToLower();
      var size = stringToManipulate.Length;
      var listOfDuplicates = new StringBuilder();

      for (var i = 0; i < size; i++)
      {
        var currentCharacter = stringToManipulate[i];
        var characterAlreadyExists = listOfDuplicates.ToString().IndexOf(currentCharacter) != -1;
        if (characterAlreadyExists)
        {
          continue;
        }

        for (var j = i + 1; j < size; j++)
        {
          characterAlreadyExists = listOfDuplicates.ToString().IndexOf(stringToManipulate[j]) != -1;
          if (currentCharacter == stringToManipulate[j] && !characterAlreadyExists)
          {
            listOfDuplicates.Append(currentCharacter);
          }
        }
      }

      return listOfDuplicates.ToString();
    }

    public static string FindUniqueCharacters(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return "";
      }

      stringToManipulate = stringToManipulate.ToLower();
      var size = stringToManipulate.Length;
      var listOfUniques = new StringBuilder();

      for (var i = 0; i < size; i++)
      {
        var currentCharacter = stringToManipulate[i];
        var characterAlreadyExists = false;

        for (var j = 0; j < size; j++)
        {
          if (currentCharacter != stringToManipulate[j] || i == j)
          {
            continue;

          }

          characterAlreadyExists = true;
          break;
        }

        if (!characterAlreadyExists)
        {
          listOfUniques.Append(currentCharacter);
        }
      }

      return listOfUniques.ToString();
    }

    public static string ReverseString(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return "";
      }

      var size = stringToManipulate.Length;
      var builder = new StringBuilder();

      for (var i = size - 1; i >= 0; i--)
      {
        builder.Append(stringToManipulate[i]);
      }

      return builder.ToString();
    }

    public static string ReverseEachWord(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return "";
      }

      var splitString = stringToManipulate.Split(" ");
      var builder = new StringBuilder();

      foreach (var str in splitString)
      {
        var strSize = str.Length;
        for (var i = strSize - 1; i >= 0; i--)
        {
          builder.Append(str[i]);
        }

        builder.Append(" ");
      }

      return builder.ToString();
    }

    public static int CountEachWord(string stringToManipulate)
    {
      return !string.IsNullOrEmpty(stringToManipulate) ? stringToManipulate.Trim().Split(" ").Length : 0;
    }

    private static string RemoveSpecialCharacters(string str)
    {
      var sb = new StringBuilder();
      foreach (var c in str.Where(c => (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')))
      { 
        sb.Append(c);
      }
      return sb.ToString().ToLower();
    }

    public static bool IsPalindrome(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return false;
      }

      stringToManipulate = RemoveSpecialCharacters(stringToManipulate);

      var size = stringToManipulate.Length;
      var index = size/2;
      var j = size - 1;

      for (var i = 0; i < index; i ++, j--)
      {
        if (stringToManipulate[i] != stringToManipulate[j])
        {
          return false;
        }
      }
      return true;
    }

    public static string MaxOccurrence(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return string.Empty;
      }

      stringToManipulate = stringToManipulate.ToLower();
      var builder = new StringBuilder();

      var size = stringToManipulate.Length;
      var maxValue = 0;
     

      for (var i = 0; i < size; i++)
      {
        var currentChar = stringToManipulate[i];
        var count = 0;
        if (builder.ToString().IndexOf(currentChar)!=-1)
        {
          continue;
        }

        for (var j = i + 1; j < size; j++)
        {
          count = currentChar == stringToManipulate[j] ? count + 1 : count;
        }

        if (count < maxValue)
        {
          continue;
        }

        if (count == maxValue)
        {
          builder.Append(currentChar);
          continue;
        }

        maxValue = count;
        builder.Remove(0, builder.Length);
        builder.Append(currentChar);
      }
      return builder.ToString();
    }
    public static string GetPossibleSubstring(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return "";
      }

      var builder = new StringBuilder();
      var size = stringToManipulate.Length;

      for (var i = 0; i <size; i++)
      {
        var newSize = stringToManipulate[i..].Length;

        for (var j = 0; j < newSize; j++)
        {
          builder.AppendLine(stringToManipulate.Substring(i, j+1));
        }
      }

      return builder.ToString();
    }

    public static string ConvertFirstLetterToUpper(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return "";
      }

      var builder = new StringBuilder();
      var splitString = stringToManipulate.Split(" ");
      
      foreach (var str in splitString)
      {
        var newString = $"{str[0].ToString().ToUpper()}{str.Remove(0, 1)} ";
        builder.Append(newString);
      }

      return builder.ToString();
    }

    public static string MultiplyStrings(string str1, string str2)
    {
      if (string.IsNullOrEmpty(str1) || string.IsNullOrEmpty(str2))
      {
        return "";
      }
      
      var isS1NegativeValue = false;
      var isS2NegativeValue = false;
      if (str1.IndexOf("-", StringComparison.Ordinal) != -1)
      {
        isS1NegativeValue = true;
        str1 = str1.Replace("-", "");
      }
      if (str2.IndexOf("-", StringComparison.Ordinal) != -1)
      {
        isS2NegativeValue = true;
        str2 = str2.Replace("-", "");
      }
      var size1 = str1.Length;
      var size2 = str2.Length;
     
      BigInteger finalSum = 0;
      for (var i = 0; i < size2; i++)
      {
        BigInteger sum = 0;
        var digit1 = str2[i] - '0';
        for (var j = 0; j < size1; j++)
        {
          var digit2 = str1[j] - '0';
         
          BigInteger result = 1;
          var exponent = size1 - 1 - j;
         
          while (exponent != 0)
          {
            result *= 10;
            --exponent;
          }
          sum += digit2 * digit1 * result;
          Console.WriteLine($"[{digit1}, {digit2}], sum = {sum}, exponent = {result}");
        }

        BigInteger result2 = 1;
        var exponent2 = size2 - 1 - i;

        while (exponent2 != 0)
        {
          result2 *= 10;
          --exponent2;
        }
        sum *= result2;
        finalSum += sum;
        Console.WriteLine($"Final sum = {finalSum}, exponent = {result2}");
        Console.WriteLine();
      }

      var finalResult = finalSum + "";
      if (isS1NegativeValue && isS2NegativeValue || finalSum == 0)
      {
        finalResult = finalSum + "";
      }
      else if (isS1NegativeValue || isS2NegativeValue)
      {
        finalResult = "-" + finalSum;
      }
      return finalResult;
    }

    public static List<string> FindPermutation(string stringToManipulate)
    {
      if (string.IsNullOrEmpty(stringToManipulate))
      {
        return null;
      }

      var builder = new StringBuilder();
      var list = new List<string>();
      var size = stringToManipulate.Length;

      for (var i = 0; i<size; i++)
      {
        var current = stringToManipulate[i];
        builder.Append(current);

        var newS = stringToManipulate[(i + 1)..];
        var newSSize = newS.Length;
        for (var j = 0; j < newSSize; j++)
        {
          current = stringToManipulate[j];
          if (i!=j)
          {
            builder.Append(current);
          }
        }
        list.Add(builder.ToString());
        builder.Clear();
      }


      return list;
    }

    public static void SwapStrings(string str1, string str2)
    {
      if (string.IsNullOrEmpty(str1) || string.IsNullOrEmpty(str2))
      {
        return;
      }

      Console.WriteLine($"string 1 before swap => {str1}");
      Console.WriteLine($"string 2 before swap => {str2} \n");

      str1 = str2 + str1;
      str2 = str1[str2.Length..];
      str1 = str1[0..str2.Length];

      Console.WriteLine($"string 1 after swap => {str1}");
      Console.WriteLine($"string 2 after swap => {str2}");
    }
  }
}
